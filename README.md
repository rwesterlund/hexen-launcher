# HeXen Launcher

Version 1.3.1

## Introduction

This is a frontend for modern Doom source ports. The name comes from the fact that most
Doom-related names were taken for launcher and HeXen is also my personal favorite. It is
not limited to HeXen.

I created yet another launcher because I felt most of the launcher are either too simple or
too cluttered. It was also cumbersome to manually add each and every PWAD so I wanted a launcher
that could display directory trees with checkboxes for the files I wanted to use.

## Features

  * Cross-platform
  * Customizable layout through draggable and collapsible splitters.
  * Settings are saved either to the user's application settings directory or in the working directory if portable mode is selected.
  * Select PWADs and DeHackEd files from a directory tree view.
  * Files for the engine are specified by either absolute or relative path, with no need to specify search paths for the engine.
  * Launch configurations can be saved so they can later be reused for loading your favorite
  combinations of engine, command line parameters, PWADs etc.

## Building from Source

This project uses CMake and depends on Qt5. In Windows it is a good idea to set the
`Qt5_DIR` environment variable to point to a Qt5 installation,
for example `C:\Qt\5.10.0\msvc2017_64`, before running CMake.

## Installation

Just drop the executable and its' associated shared libraries anywhere you like.

## Usage

The executable can be run without parameters and provided that the Qt libraries could be
properly loaded you will be presented with the launcher UI. Currently there are no command line
parameters supported by the application.

There are three lists for displaying IWADs, additional files and saved configurations which can all
be manipulated by right-clicking and using the context menu. Elements that are not obvious will
have a tooltip that will explain what they're used for. Saved configurations can be renamed
by selecting an item and pressing F2 on the key board or whichever key your OS uses for renaming.
