#include "Launcher.hpp"
#include <QApplication>

int main(int argc, char *argv[])
{
    QApplication App(argc, argv);
	App.setStyleSheet("QMessageBox { messagebox-text-interaction-flags: 5; }");
    Launcher launcher;
    launcher.show();

    return App.exec();
}
