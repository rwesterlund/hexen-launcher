#include "Launcher.hpp"
#include "FileTreeModel.hpp"
#include "FileTreeItem.hpp"
#include <QString>
#include <QStringList>
#include <QSettings>
#include <QSplitter>
#include <QBoxLayout>
#include <QLabel>
#include <QSlider>
#include <QLineEdit>
#include <QComboBox>
#include <QPushButton>
#include <QCheckBox>
#include <QListWidget>
#include <QTreeView>
#include <QStandardItemModel>
#include <QStandardItem>
#include <QHeaderView>
#include <QProcess>
#include <QFileDialog>
#include <QMenu>
#include <QAction>
#include <QDirIterator>
#include <QDataStream>
#include <QMessageBox>
#include <QMenuBar>
#include <QMenu>
#include <chrono>

Launcher::Launcher(QWidget *parent) :
    QMainWindow(parent),
	m_classRandomizer(0, 2),
	m_writeSettings(true)
{
	unsigned int seed = std::chrono::system_clock::now().time_since_epoch().count();
	m_randomGenerator = std::default_random_engine{seed};

	setWindowTitle(applicationName);

    resize(640, 480);

	// Set up central widget with a vertical layout for splitter and launch button.
	m_centralWidget = std::make_unique<QWidget>();
	m_centralWidgetLayout = std::make_unique<QBoxLayout>(QBoxLayout::TopToBottom);
	m_centralWidget->setLayout(m_centralWidgetLayout.get());
	setCentralWidget(m_centralWidget.get());

	// Set up the vertical section of the central widget.
	m_columnSplitter = std::make_unique<QSplitter>(Qt::Horizontal);
	m_launchRow = std::make_unique<QWidget>();
	m_launchRowLayout = std::make_unique<QBoxLayout>(QBoxLayout::LeftToRight);
	m_launchRowLayout->setMargin(0);
	m_launchRow->setLayout(m_launchRowLayout.get());
	m_centralWidgetLayout->addWidget(m_columnSplitter.get());
	m_centralWidgetLayout->addWidget(m_launchRow.get());

	// Set up the launch row.
	m_launch = std::make_unique<QPushButton>(tr("Launch"));
	connect(m_launch.get(), SIGNAL(clicked(bool)), this, SLOT(runPressed(bool)));
	m_showCmdLine = std::make_unique<QPushButton>(tr("Cmd?"));
	connect(m_showCmdLine.get(), SIGNAL(clicked(bool)), this, SLOT(showCmdLinePressed(bool)));
	m_showCmdLine->setMaximumWidth(m_showCmdLine->width());
	m_showCmdLine->setToolTip(tr("Displays the parameters to launch the engine process. They can be selected and copied."));
	m_launchRowLayout->addWidget(m_launch.get());
	m_launchRowLayout->addWidget(m_showCmdLine.get());

	m_launchRowLayout->setStretch(0, 1);
	m_launchRowLayout->setStretch(1, 0);

	m_centralWidgetLayout->setStretch(0, 1);
	m_centralWidgetLayout->setStretch(1, 0);

	// Set up the column splitter.
    m_leftColumn = std::make_unique<QWidget>();
    m_middleColumn = std::make_unique<QWidget>();
    m_rightColumn = std::make_unique<QWidget>();
    m_leftColumnLayout = std::make_unique<QBoxLayout>(QBoxLayout::TopToBottom);
    m_middleColumnLayout = std::make_unique<QBoxLayout>(QBoxLayout::TopToBottom);
    m_rightColumnLayout = std::make_unique<QBoxLayout>(QBoxLayout::TopToBottom);
	m_leftColumnLayout->setMargin(0);
	m_middleColumnLayout->setMargin(0);
	m_rightColumnLayout->setMargin(0);
    m_leftColumn->setLayout(m_leftColumnLayout.get());
    m_middleColumn->setLayout(m_middleColumnLayout.get());
    m_rightColumn->setLayout(m_rightColumnLayout.get());

	m_columnSplitter->addWidget(m_leftColumn.get());
	m_columnSplitter->addWidget(m_middleColumn.get());
	m_columnSplitter->addWidget(m_rightColumn.get());

    QList<int> sizes;
    for (int i = 0; i < 3; ++i)
    {
        sizes << 640/3;
		m_columnSplitter->setStretchFactor(i, 1);
    }
	m_columnSplitter->setSizes(sizes);

	// Left column
	m_iwadsLabel = std::make_unique<QLabel>(tr("IWADs"));
	m_iwadsLabel->setMinimumWidth(32);
	m_iwads = std::make_unique<QListWidget>();
	m_iwads->setContextMenuPolicy(Qt::CustomContextMenu);
	connect(m_iwads.get(), SIGNAL(currentItemChanged(QListWidgetItem *, QListWidgetItem *)), this, SLOT(iwadChanged(QListWidgetItem *, QListWidgetItem *)));
	connect(m_iwads.get(), SIGNAL(customContextMenuRequested(const QPoint&)), this, SLOT(iwadsContextMenu(const QPoint&)));
	m_iwads->setToolTip(tr("Right-click for context menu."));

	m_engineLayout = std::make_unique<QBoxLayout>(QBoxLayout::LeftToRight);
	m_engineLabel = std::make_unique<QLabel>(tr("Engine"));
	m_enginePath = std::make_unique<QComboBox>();
	m_enginePath->setEditable(true);
	m_enginePath->setSizeAdjustPolicy(QComboBox::SizeAdjustPolicy::AdjustToContentsOnFirstShow);
	m_enginePath->setStyleSheet("* QComboBox QAbstractItemView { min-width: 300px; }");
	m_browseEngine = std::make_unique<QPushButton>(tr("..."));
	m_browseEngine->setMaximumWidth(32);
	connect(m_browseEngine.get(), SIGNAL(clicked(bool)), this, SLOT(browseEnginePressed(bool)));
	m_engineLayout->addWidget(m_engineLabel.get());
	m_engineLayout->addWidget(m_enginePath.get());
	m_engineLayout->addWidget(m_browseEngine.get());
	m_engineLayout->setStretch(1, 1);

	m_skillLayout = std::make_unique<QBoxLayout>(QBoxLayout::LeftToRight);
	m_skillLabel = std::make_unique<QLabel>(tr("Skill"));
	m_skillSlider = std::make_unique<QSlider>(Qt::Horizontal);
	m_skillSlider->setRange(1, 5);
	m_skillSlider->setValue(4);
	m_skillSlider->setToolTip(tr("Will start the game using this difficulty. Only applies if a starting map is specified."));
	m_skillLayout->addWidget(m_skillLabel.get());
	m_skillLayout->addWidget(m_skillSlider.get());

	m_mapLayout = std::make_unique<QBoxLayout>(QBoxLayout::LeftToRight);
	m_mapLabel = std::make_unique<QLabel>(tr("Map"));
	m_mapName = std::make_unique<QComboBox>();
	m_mapName->setToolTip(tr("Will start the game on this map if specified."));
	m_mapName->setStyleSheet("* QComboBox QAbstractItemView { min-width: 300px; }");
	m_mapLayout->addWidget(m_mapLabel.get());
	m_mapLayout->addWidget(m_mapName.get());
	m_mapLayout->setStretch(1, 1);
	populateMaps(QString{});

	m_classLayout = std::make_unique<QBoxLayout>(QBoxLayout::LeftToRight);
	m_classLabel = std::make_unique<QLabel>(tr("Class"));
	m_className = std::make_unique<QComboBox>();
	m_className->setToolTip(tr("Will start the game using this class. Only applies if a starting map is specified."));
	m_className->addItems({tr("Fighter"), tr("Cleric"), tr("Mage"), tr("Random")});
	m_className->setCurrentIndex(3);
	m_className->setEditable(true);
	m_className->setInsertPolicy(QComboBox::NoInsert);
	m_classLayout->addWidget(m_classLabel.get());
	m_classLayout->addWidget(m_className.get());
	m_classLayout->setStretch(1, 1);

	m_parametersLayout = std::make_unique<QBoxLayout>(QBoxLayout::LeftToRight);
	m_parametersLayout->setAlignment(Qt::AlignLeft);
	m_zdoomStrictLabel = std::make_unique<QLabel>(tr("ZDoom Strict"));
	m_zdoomStrictCheckBox = std::make_unique<QCheckBox>();
	m_zdoomStrictCheckBox->setTristate();
	m_zdoomStrictCheckBox->setToolTip(tr("Provides ZDoom command line options setting compatflags and dmflags so the IWADs plays closely to the original game.\n"
											"Checked: Close to original gameplay, Partially checked: Not applied, Unchecked: All flags set to default value of 0."));
	m_parametersLayout->addWidget(m_zdoomStrictLabel.get());
	m_parametersLayout->addWidget(m_zdoomStrictCheckBox.get());

	m_commandLineLabel = std::make_unique<QLabel>(tr("Command Line"));
	m_commandLine = std::make_unique<QLineEdit>();

	m_leftColumnLayout->addWidget(m_iwadsLabel.get());
	m_leftColumnLayout->addWidget(m_iwads.get());
	m_leftColumnLayout->addLayout(m_engineLayout.get());
	m_leftColumnLayout->addLayout(m_skillLayout.get());
	m_leftColumnLayout->addLayout(m_mapLayout.get());
	m_leftColumnLayout->addLayout(m_classLayout.get());
	m_leftColumnLayout->addLayout(m_parametersLayout.get());
	m_leftColumnLayout->addWidget(m_commandLineLabel.get());
	m_leftColumnLayout->addWidget(m_commandLine.get());

	// Middle column
	m_filesLabel = std::make_unique<QLabel>(tr("Additional Files"));
	m_files = std::make_unique<QTreeView>();
	m_filesModel = new FileTreeModel(m_files.get());
	m_files->setModel(m_filesModel);
	m_filesModel->setHeaderData(0, Qt::Horizontal, tr("File"));
	m_filesModel->setHeaderData(1, Qt::Horizontal, tr("Priority"));
	m_files->header()->resizeSection(0, 180);
	m_files->header()->resizeSection(1, 30);
	m_files->setSortingEnabled(true);
	m_files->sortByColumn(0, Qt::AscendingOrder);
	m_files->setContextMenuPolicy(Qt::CustomContextMenu);
	connect(m_files.get(), SIGNAL(customContextMenuRequested(const QPoint&)), this, SLOT(filesContextMenu(const QPoint&)));
	m_files->setToolTip(tr("Right-click for context menu.\nInclude a file by checking it. After a file has been checked the load order priority can be modified."));
	m_middleColumnLayout->addWidget(m_filesLabel.get());
	m_middleColumnLayout->addWidget(m_files.get());

	// Right column
    m_savedConfLabel = std::make_unique<QLabel>(tr("Saved Configurations"));
	m_savedConf = std::make_unique<QListWidget>();
	m_savedConf->setEditTriggers(QAbstractItemView::EditKeyPressed);
	m_savedConf->setContextMenuPolicy(Qt::CustomContextMenu);
	m_savedConf->setDragEnabled(true);
	m_savedConf->setDragDropMode(QAbstractItemView::InternalMove);
	connect(m_savedConf.get(), SIGNAL(customContextMenuRequested(const QPoint&)), this, SLOT(savedConfContextMenu(const QPoint&)));
	connect(m_savedConf.get(), SIGNAL(itemDoubleClicked(QListWidgetItem *)), this, SLOT(configurationDoubleClicked(QListWidgetItem *)));
	m_savedConf->setToolTip(tr("Right-click for context menu."));

	m_savedConfMoveUpAction = std::make_unique<QAction>(tr("Move this item up the list\tCtrl+Up"), m_savedConf.get());
	m_savedConfMoveDownAction = std::make_unique<QAction>(tr("Move this item down the list\tCtrl+Down"), m_savedConf.get());
	connect(m_savedConfMoveUpAction.get(), SIGNAL(triggered()), this, SLOT(moveConfigurationUp()));
	m_savedConfMoveUpAction->setShortcut(Qt::CTRL + Qt::Key_Up);
	m_savedConf->addAction(m_savedConfMoveUpAction.get());

	connect(m_savedConfMoveDownAction.get(), SIGNAL(triggered()), this, SLOT(moveConfigurationDown()));
	m_savedConfMoveDownAction->setShortcut(QKeySequence{Qt::CTRL + Qt::Key_Down});
	m_savedConf->addAction(m_savedConfMoveDownAction.get());

	m_rightColumnLayout->addWidget(m_savedConfLabel.get());
	m_rightColumnLayout->addWidget(m_savedConf.get());

	// Menu bar with settings and about item.
	m_settingsMenu = menuBar()->addMenu(tr("Settings"));
	m_settingPortableMode = std::make_unique<QAction>(tr("Portable Mode"), this);
	m_settingPortableMode->setCheckable(true);
	m_settingsMenu->addAction(m_settingPortableMode.get());

	m_aboutAction = std::make_unique<QAction>(tr("About"), this);
	connect(m_aboutAction.get(), SIGNAL(triggered()), this, SLOT(showAboutWindow()));
	menuBar()->addAction(m_aboutAction.get());


	loadSettings();
}

Launcher::~Launcher()
{
	saveSettings();
}

// Signals
void Launcher::runPressed(bool checked)
{
    QProcess gameProcess{this};

	QString path;
	QStringList arguments;
	QString workingDirectory;

	if (assembleCommandLine(path, arguments, workingDirectory))
		gameProcess.startDetached(path, arguments, workingDirectory);
}

void Launcher::showCmdLinePressed(bool checked)
{
	QString path;
	QStringList arguments;
	QString workingDirectory;

	if (assembleCommandLine(path, arguments, workingDirectory))
	{
		if (path.contains(' '))
			path = "\"" + path + "\"";
		for (int i = 0; i < arguments.size(); ++i) {
			if (arguments[i].contains(' '))
				arguments[i] = "\"" + arguments[i] + "\"";
		}
		QMessageBox::information(this, tr("Launch parameters"), tr("%1 %2\n\nWorking directory: %3").arg(path, arguments.join(' '), workingDirectory));
	}
}

void Launcher::browseEnginePressed(bool checked)
{
#if defined(_WIN32)
	QString path = QFileDialog::getOpenFileName(this, tr("Locate Engine"), m_enginePath->currentText(), tr("Executable (*.exe)"));
#else
	QString path = QFileDialog::getOpenFileName(this, tr("Locate Engine"), m_enginePath->currentText(), tr("Executable (*)"));
#endif
	setEnginePath(path);
}

void Launcher::iwadChanged(QListWidgetItem *current, QListWidgetItem *previous)
{
	if (current)
		populateMaps(current->text());
	else
		populateMaps(QString{});
}

void Launcher::iwadsContextMenu(const QPoint &pos)
{
	QMenu menu(this);

	QAction setIwadFolderAction{tr("Set IWAD folder"), this};
	QAction setRelativeIwadFolderAction{tr("Set relative IWAD folder"), this};
	
	connect(&setIwadFolderAction, SIGNAL(triggered()), this, SLOT(setIwadFolder()));
	menu.addAction(&setIwadFolderAction);

	connect(&setRelativeIwadFolderAction, SIGNAL(triggered()), this, SLOT(setRelativeIwadFolder()));
	menu.addAction(&setRelativeIwadFolderAction); 

	menu.exec(m_iwads->mapToGlobal(pos));
}

void Launcher::filesContextMenu(const QPoint &pos)
{
	QMenu menu(this);

	QAction addFolderAction{tr("Add folder"), this};
	QAction addFolderRelativeAction{tr("Add folder relative path"), this};
	QAction removeFolderAction{tr("Remove folder"), this};
	QAction expandTreeAction{tr("Expand tree"), this};
	QAction collapseTreeAction{tr("Collapse tree"), this};
	QAction uncheckAll{tr("Uncheck all"), this};

	connect(&addFolderAction, SIGNAL(triggered()), this, SLOT(addFileFolder()));
	menu.addAction(&addFolderAction);

	connect(&addFolderRelativeAction, SIGNAL(triggered()), this, SLOT(addRelativeFileFolder()));
	menu.addAction(&addFolderRelativeAction);

	// Populate menu items based on selection.
	QModelIndexList selection = m_files->selectionModel()->selectedRows();
	if (!selection.isEmpty()) {
		FileTreeItem *item = m_filesModel->item(selection.first());

		// A directory root.
		if (item->isTopLevel()) {
			connect(&removeFolderAction, SIGNAL(triggered()), this, SLOT(removeFileFolder()));
			menu.addAction(&removeFolderAction);
		}

		// Subdirectory
		if (item->childCount() > 0) {
			connect(&expandTreeAction, SIGNAL(triggered()), this, SLOT(expandFileTree()));
			menu.addAction(&expandTreeAction);

			connect(&collapseTreeAction, SIGNAL(triggered()), this, SLOT(collapseFileTree()));
			menu.addAction(&collapseTreeAction);
		}
	}

	// If there are items enable uncheck all option.
	if (m_filesModel->topLevelItemCount() > 0) {
		connect(&uncheckAll, SIGNAL(triggered()), this, SLOT(uncheckAllFiles()));
		menu.addAction(&uncheckAll);
	}

	menu.exec(m_files->mapToGlobal(pos));
}

void Launcher::savedConfContextMenu(const QPoint &pos)
{
	QMenu menu(this);

	QAction saveNewAction{tr("Save new"), this};
	QAction updateItemAction{tr("Update item with current settings"), this};
	QAction removeConfigurationAction{tr("Remove configuration"), this};

	connect(&saveNewAction, SIGNAL(triggered()), this, SLOT(saveNewConfiguration()));
	menu.addAction(&saveNewAction);

	// Populate menu items based on selection.
	QList<QListWidgetItem *> selection = m_savedConf->selectedItems();
	if (!selection.isEmpty()) {
		QListWidgetItem *item = selection.first();

		connect(&updateItemAction, SIGNAL(triggered()), this, SLOT(updateConfiguration()));
		menu.addAction(&updateItemAction);

		connect(&removeConfigurationAction, SIGNAL(triggered()), this, SLOT(removeConfiguration()));
		menu.addAction(&removeConfigurationAction);

		menu.addAction(m_savedConfMoveUpAction.get());
		menu.addAction(m_savedConfMoveDownAction.get());
	}

	menu.exec(m_savedConf->mapToGlobal(pos));
}

void Launcher::setIwadFolder()
{
	QString path = QFileDialog::getExistingDirectory(this, tr("Locate folder containing IWADs"));
	populateIwads(path);
}

void Launcher::setRelativeIwadFolder()
{
	QString path = QFileDialog::getExistingDirectory(this, tr("Locate folder containing IWADs"));
	QDir dir{QDir::currentPath()};
	QString relativePath = dir.relativeFilePath(path);

	if (relativePath.isEmpty()) {
		QMessageBox::critical(this, tr("Invalid path"),
			tr("A relative path for folder \"%1\" could not be found for base directory \"%2\".").arg(path, dir.absolutePath()));
	} else {
		populateIwads(relativePath);
	}
}

void Launcher::addFileFolder()
{
	QString path = QFileDialog::getExistingDirectory(this, tr("Locate folder containing additional files"));
	populateFileFolder(path);
}

void Launcher::addRelativeFileFolder()
{
	QString path = QFileDialog::getExistingDirectory(this, tr("Locate folder containing additional files"));
	QDir dir{QDir::currentPath()};
	QString relativePath = dir.relativeFilePath(path);

	if (relativePath.isEmpty()) {
		QMessageBox::critical(this, tr("Invalid path"),
			tr("A relative path for folder \"%1\" could not be found for base directory \"%2\".").arg(path, dir.absolutePath()));
	} else {
		populateFileFolder(relativePath);
	}
}

void Launcher::removeFileFolder()
{
	QModelIndexList selection = m_files->selectionModel()->selectedRows();
	if (!selection.isEmpty()) {
		FileTreeItem *item = m_filesModel->item(selection.first());
		if (item->isTopLevel())
			delete item;
	}
}

void Launcher::expandFileTree()
{
	QModelIndexList selection = m_files->selectionModel()->selectedRows();
	if (!selection.isEmpty()) {
		FileTreeItem *item = m_filesModel->item(selection.first());
		if (item->childCount() > 0) {
			expandOrCollapseTree(item, true);
		}
	}
}

void Launcher::collapseFileTree()
{
	QModelIndexList selection = m_files->selectionModel()->selectedRows();
	if (!selection.isEmpty()) {
		FileTreeItem *item = m_filesModel->item(selection.first());
		if (item->childCount() > 0) {
			expandOrCollapseTree(item, false);
		}
	}
}

void Launcher::uncheckAllFiles()
{
	for (int i = 0; i < m_filesModel->topLevelItemCount(); ++i)
		uncheckFileTree(m_filesModel->topLevelItem(i));
}

void Launcher::saveNewConfiguration()
{
	QListWidgetItem *item = new QListWidgetItem{tr("Untitled")};
	item->setData(Qt::UserRole, calculateConfiguration());
	item->setFlags(item->flags() | Qt::ItemIsEditable);
	connect(m_savedConf.get(), SIGNAL(itemDoubleClicked(QListWidgetItem *)), this, SLOT(configurationDoubleClicked(QListWidgetItem *)));
	m_savedConf->addItem(item);
}

void Launcher::updateConfiguration()
{
	QList<QListWidgetItem *> selection = m_savedConf->selectedItems();
	if (!selection.isEmpty()) {
		QListWidgetItem *item = selection.first();
		item->setData(Qt::UserRole, calculateConfiguration());
	}
}

void Launcher::removeConfiguration()
{
	QList<QListWidgetItem *> selection = m_savedConf->selectedItems();
	if (!selection.isEmpty())
		delete selection.first();
}

void Launcher::moveConfigurationUp()
{
	QList<QListWidgetItem *> selection = m_savedConf->selectedItems();
	if (!selection.isEmpty()) {
		int row = m_savedConf->currentRow();
		QListWidgetItem *item = m_savedConf->takeItem(row);
		m_savedConf->insertItem(row - 1, item);
		m_savedConf->setCurrentRow(row - 1);
	}
}

void Launcher::moveConfigurationDown()
{
	QList<QListWidgetItem *> selection = m_savedConf->selectedItems();
	if (!selection.isEmpty()) {
		int row = m_savedConf->currentRow();
		QListWidgetItem *item = m_savedConf->takeItem(row);
		m_savedConf->insertItem(row + 1, item);
		m_savedConf->setCurrentRow(row + 1);
	}
}

void Launcher::configurationDoubleClicked(QListWidgetItem *item)
{
	applyConfiguration(item->data(Qt::UserRole).toByteArray(), true);
}

void Launcher::showSettingsMenu()
{
}

void Launcher::showAboutWindow()
{
	QMessageBox::about(this, tr("About"), tr("%1 v1.3.1\n\nThis software uses Qt. The license can be found in the licenses subdirectory.").arg(applicationName));
}

// Private methods
void Launcher::loadSettings()
{
	QFileInfo settingsFile{localSettingsFileName};
	if (settingsFile.exists() && settingsFile.isFile()) {
		QSettings settings{localSettingsFileName, QSettings::IniFormat};
		loadSettingsUsingFile(settings);
		m_settingPortableMode->setChecked(true);
	} else {
		QSettings settings{QSettings::IniFormat, QSettings::UserScope, organizationName, applicationName};
		loadSettingsUsingFile(settings);
		m_settingPortableMode->setChecked(false);
	}
}

void Launcher::loadSettingsUsingFile(QSettings &settings)
{
	// Handle the case when an older version is run using settings from a newer version.
	unsigned int revision = settings.value("revision", 0).toInt();
	if (revision != 0 && revision > settingsRevision) {
		QMessageBox::StandardButton choice =
			QMessageBox::warning(this,
				tr("Incompatible program settings"),
				tr("The settings for this application were made using a newer version. Would you like to overwrite them with settings compatible for this version?"),
				QMessageBox::Yes | QMessageBox::No);
		m_writeSettings = choice == QMessageBox::Yes;
	}

	settings.beginGroup("UIState");
	restoreState(settings.value("mainWindow", QByteArray()).toByteArray());
	restoreGeometry(settings.value("mainWindowGeometry", QByteArray()).toByteArray());
	m_columnSplitter->restoreState(settings.value("columnSplitter", QByteArray()).toByteArray());
	m_files->header()->restoreState(settings.value("filesTreeHeader", QByteArray()).toByteArray());
	settings.endGroup();

	settings.beginGroup("CurrentValues");
	m_iwadsPath = settings.value("iwadsPath", "").toString();
	QString currentIwad = settings.value("selectedIwad", "").toString();
	int engines = settings.beginReadArray("enginePaths");
	for (int i = 0; i < engines; ++i) {
		settings.setArrayIndex(i);
		QString enginePath = settings.value("enginePaths", "").toString();
		if (!enginePath.isEmpty())
			m_enginePath->addItem(enginePath);
	}
	settings.endArray();
	QByteArray configuration = settings.value("configuration", QByteArray()).toByteArray();
	settings.endGroup();
	populateIwads(m_iwadsPath);

	settings.beginGroup("FileLocations");
	int fileLocations = settings.beginReadArray("paths");
	for (int i = 0; i < fileLocations; ++i) {
		settings.setArrayIndex(i);
		populateFileFolder(settings.value("paths", "").toString());
	}
	settings.endArray();
	settings.endGroup();

	if (revision > 1) { // Discard configurations from incompatible versions 0 & 1.
		settings.beginGroup("SavedConfigurations");
		int configurations = settings.beginReadArray("configurations");
		for (int i = 0; i < configurations; ++i) {
			settings.setArrayIndex(i);
			QString name = settings.value("configurations", "").toString();
			QByteArray data = settings.value("configurationData", QByteArray{}).toByteArray();

			if (revision == 2)
				migrateStrictCheckStateToTriState(data);

			QListWidgetItem *item = new QListWidgetItem{name};
			item->setData(Qt::UserRole, data);
			item->setFlags(item->flags() | Qt::ItemIsEditable);
			m_savedConf->addItem(item);
		}
		settings.endArray();
		settings.endGroup();
	} else if (revision != 0) {
		QMessageBox::StandardButton choice =
			QMessageBox::warning(this,
				tr("Incompatible program settings"),
				tr("Configurations could not be loaded due to incompatible layouts. Would you like to overwrite your old settings?"),
				QMessageBox::Yes | QMessageBox::No);
		m_writeSettings = choice == QMessageBox::Yes;
	}

	applyConfiguration(configuration, false);
}

void Launcher::saveSettings()
{
	if (!m_writeSettings)
		return;

	if (m_settingPortableMode->isChecked()) {
		QSettings settings{localSettingsFileName, QSettings::IniFormat};
		saveSettingsUsingFile(settings);
	} else {
		// If the user switched to non-portable mode the local settings file need
		// to be deleted so it is not read on next startup.
		QFile settingsFile{localSettingsFileName};
		if (settingsFile.exists())
			settingsFile.remove();

		QSettings settings{QSettings::IniFormat, QSettings::UserScope, organizationName, applicationName};
		saveSettingsUsingFile(settings);
	}
}

void Launcher::saveSettingsUsingFile(QSettings &settings)
{
	settings.setValue("revision", settingsRevision);

	settings.beginGroup("UIState");
	settings.setValue("mainWindow", saveState());
	settings.setValue("mainWindowGeometry", saveGeometry());
	settings.setValue("columnSplitter", m_columnSplitter->saveState());
	settings.setValue("filesTreeHeader", m_files->header()->saveState());
	settings.endGroup();

	settings.beginGroup("CurrentValues");
	settings.setValue("iwadsPath", m_iwadsPath);
	settings.beginWriteArray("enginePaths");
	for (int i = 0; i < m_enginePath->count(); ++i) {
		settings.setArrayIndex(i);
		settings.setValue("enginePaths", m_enginePath->itemText(i));
	}
	settings.endArray();
	settings.setValue("configuration", calculateConfiguration());
	settings.endGroup();

	settings.beginGroup("FileLocations");
	settings.beginWriteArray("paths");
	for (int i = 0; i < m_filesModel->topLevelItemCount(); ++i) {
		settings.setArrayIndex(i);
		settings.setValue("paths", m_filesModel->topLevelItem(i)->text(0));
	}
	settings.endArray();
	settings.endGroup();

	settings.beginGroup("SavedConfigurations");
	settings.beginWriteArray("configurations");
	for (int i = 0; i < m_savedConf->count(); ++i) {
		settings.setArrayIndex(i);
		QListWidgetItem *item = m_savedConf->item(i);
		settings.setValue("configurations", item->text());
		settings.setValue("configurationData", item->data(Qt::UserRole));
	}
	settings.endArray();
	settings.endGroup();
}

bool Launcher::assembleCommandLine(QString &path, QStringList &arguments, QString &workingDirectory)
{
	path = m_enginePath->currentText();
	EngineFamily engineFamily = engineFamilyForBinary(path);
	QString iwad = selectedIwad();

	if (iwad.isEmpty()) {
		QMessageBox::critical(this, tr("Error"), tr("No IWAD has been selected!"));
		return false;
	}

	// IWAD
	QStringList iwadPathComponents;
	iwadPathComponents << m_iwadsPath << iwad;
	arguments << "-iwad" << iwadPathComponents.join('/');

	// Add additional files according to priority and type; PWADs or DEH/BEX.
	QList<QPair<QString, int>> files = checkedFiles();
	bool writingFiles = false;
	for (const QPair<QString, int> &file : files) {
		QString fileLowered = file.first.toLower();
		if (fileLowered.endsWith(".deh") || fileLowered.endsWith(".bex")) {
			arguments << "-deh" << file.first;
			writingFiles = false;
		} else {
			if (!writingFiles) {
				arguments << "-file";
				writingFiles = true;
			}
			arguments << file.first;
		}
	}

	if (m_mapName->currentIndex() > 0) {
		arguments << "-skill" << QString::number(m_skillSlider->value());
		arguments << "-warp" << m_mapList.warpForIwadAndListIndex(iwad, m_mapName->currentIndex() - 1).split(' ');
		
		// Apply player class.
		QString loweredIwad = iwad.toLower();
		bool considerPlayerClass =
			engineFamily == EngineFamily::ZDoom
			|| (engineFamily == EngineFamily::Chocolate
				&& (loweredIwad == "hexen.wad" || loweredIwad == "hexdd.wad"));

		if (considerPlayerClass) {
			if (engineFamily == EngineFamily::Chocolate) {
				int selectedClass = m_className->currentIndex();
				if (selectedClass < 0 || selectedClass > 2)
					selectedClass = m_classRandomizer(m_randomGenerator);
				arguments << "-class" << QString::number(selectedClass);
			} else if (engineFamily == EngineFamily::ZDoom) {
				QString selectedClass = m_className->currentText();
				arguments << "+playerclass" << selectedClass;
			}
		}
	}
	if (!m_commandLine->text().isEmpty())
		arguments << m_commandLine->text().simplified().split(' ');

	if (engineFamily == EngineFamily::ZDoom && m_zdoomStrictCheckBox->checkState() != Qt::PartiallyChecked) {
		if (m_zdoomStrictCheckBox->checkState() == Qt::Checked) {
			arguments << "+set" << "compatflags" << "-1172751401";
			arguments << "+set" << "compatflags2" << "11";

			QString iwadLowered = iwad.toLower();
			if (iwadLowered == "heretic.wad") {
				arguments << "+set" << "dmflags" << "4259840"; // No jump or crouch.
			} else if (iwadLowered == "hexen.wad" || iwadLowered == "hexdd.wad") {
				arguments << "+set" << "dmflags" << "4227088"; // No crouch.
			} else {
				arguments << "+set" << "dmflags" << "4521984"; // No vertical aim, jump or crouch.
			}
		} else {
			arguments << "+set" << "compatflags" << "0";
			arguments << "+set" << "compatflags2" << "0";
			arguments << "+set" << "dmflags2" << "0";
		}
	}

	QFileInfo pathInfo{path};
	workingDirectory = pathInfo.absoluteDir().absolutePath();

	return true;
}

Launcher::EngineFamily Launcher::engineFamilyForBinary(const QString &path)
{
	QFileInfo fileInfo{path};
	QString filename = fileInfo.baseName().toLower();

	if (filename.contains("zdoom") || filename.contains("zandronum"))
		return EngineFamily::ZDoom;
	else if (filename.contains("chocolate"))
		return EngineFamily::Chocolate;

	return EngineFamily::Unknown;
}

void Launcher::setEnginePath(const QString &path)
{
	int index = m_enginePath->findText(path);
	if (index >= 0) {
		m_enginePath->setCurrentIndex(index);
	} else {
		if (!path.isEmpty())
			m_enginePath->addItem(path);
		m_enginePath->setCurrentText(path);
	}
}

QString Launcher::selectedIwad()
{
	QString selectedIwad;
	QList<QListWidgetItem *> iwads = m_iwads->selectedItems();
	if (!iwads.isEmpty())
		selectedIwad = iwads.first()->text();
	return selectedIwad;
}

QList<QPair<QString, int>> Launcher::checkedFiles()
{
	QList<QPair<QString, int>> results;
	QStringList prefix;

	for (int i = 0; i < m_filesModel->topLevelItemCount(); ++i)
		checkedFilesRecursive(m_filesModel->topLevelItem(i), prefix, results);

	std::sort(results.begin(), results.end(),
		[] (QPair<QString, int> &left, QPair<QString, int> &right) -> bool {
			return left.second > right.second;
		});

	return results;
}

void Launcher::checkedFilesRecursive(FileTreeItem *item, QStringList &prefix, QList<QPair<QString, int>> &results)
{
	prefix.append(item->text(0));

	if (item->checkState(0) == Qt::Checked)
		results.append(QPair<QString, int>(prefix.join('/'), item->data(1, Qt::EditRole).toInt()));

	if (item->childCount() > 0) {
		// Subtree
		for (int i = 0; i < item->childCount(); ++i)
			checkedFilesRecursive(item->child(i), prefix, results);
	}

	prefix.removeLast();
}

void Launcher::checkFiles(QList<QPair<QString, int>> files, QStringList &rejected)
{
	QStringList results;
	QStringList suffix;

	for (const QPair<QString, int> &file : files) {
		bool foundFile = false;
		for (int i = 0; i < m_filesModel->topLevelItemCount(); ++i) {
			if (checkFilesRecursive(m_filesModel->topLevelItem(i), QStringRef{&file.first}, file.second)) {
				foundFile = true;
				break;
			}
		}
		if (!foundFile)
			rejected.append(file.first);
	}
}

bool Launcher::checkFilesRecursive(FileTreeItem *item, QStringRef file, int priority)
{
	QString itemText = item->text(0);
	if (file.startsWith(itemText)) {
		QStringRef nextSegment = file.right(file.size() - itemText.size());

		if (nextSegment.isEmpty()) {
			m_filesModel->setCheckState(item, 0, Qt::Checked);
			m_filesModel->setData(m_filesModel->index(item, 1), priority);
			return true;
		} else if (item->childCount() > 0) {
			// Continue recursing down the tree.
			nextSegment = nextSegment.right(nextSegment.size() - 1); // Remove separator
			for (int i = 0; i < item->childCount(); ++i) {
				bool found = checkFilesRecursive(item->child(i), nextSegment, priority);
				if (found) {
					m_filesModel->setExpanded(item, true);
					return true;
				}
			}
		}
	}

	return false;
}

void Launcher::applyConfiguration(const QByteArray &configuration, bool reportErrors)
{
	if (configuration.isEmpty())
		return;

	QDataStream stream{configuration};

	QString enginePath;
	QString iwad;
	qint8 skill;
	QString selectedMap;
	qint8 strictCheckState;
	QString commandLine;
	QList<QPair<QString, int>> files;
	QString className;

	stream >> enginePath;
	stream >> iwad;
	stream >> skill;
	stream >> selectedMap;
	stream >> strictCheckState;
	stream >> commandLine;
	stream >> files;
	stream >> className;

	setEnginePath(enginePath);

	QStringList rejected;

	// Select the IWAD.
	bool foundIwad = false;
	for (int i = 0; i < m_iwads->count(); ++i) {
		QListWidgetItem *item = m_iwads->item(i);
		if (item->text() == iwad) {
			item->setSelected(true);
			foundIwad = true;
		}
	}
	if (!foundIwad)
		rejected.append(iwad);

	m_skillSlider->setValue(static_cast<int>(skill));
	populateMaps(iwad);
	m_mapName->setCurrentIndex(selectedMap.toInt());
	if (className.isEmpty()) {
		m_className->setCurrentIndex(3); // "Random"
	} else {
		int classIndex = m_className->findText(className);
		if (classIndex >= 0)
			m_className->setCurrentIndex(classIndex);
		else
			m_className->setCurrentText(className);
	}
	m_zdoomStrictCheckBox->setCheckState(static_cast<Qt::CheckState>(strictCheckState));
	m_commandLine->setText(commandLine);

	uncheckAllFiles();
	checkFiles(files, rejected);

	// If there were errors or incompatibilities display an appriopriate dialog.
	if (reportErrors && !rejected.isEmpty()) {
		QMessageBox::warning(this, tr("Missing files"),
			tr("The following files included in the configuration could not be located through the file list:\n\n%1"
			"\n\nYou may update the configuration to ignore these files or set new paths.").arg(rejected.join('\n')));
	}
}

QByteArray Launcher::calculateConfiguration()
{
	QByteArray result;
	QDataStream stream{&result, QIODevice::WriteOnly};

	stream << m_enginePath->currentText();
	stream << selectedIwad();
	stream << static_cast<qint8>(m_skillSlider->value());
	stream << QString::number(m_mapName->currentIndex());
	stream << static_cast<qint8>(m_zdoomStrictCheckBox->checkState());
	stream << m_commandLine->text();
	stream << checkedFiles();
	stream << m_className->currentText();

	return result;
}

void Launcher::populateIwads(const QString &path)
{
	if (!path.isEmpty()) { 
		m_iwads->clear();
		m_iwadsPath = path;
		m_iwadsLabel->setText(QString{"%1 (%2)"}.arg(tr("IWADs"), m_iwadsPath));
		m_iwadsLabel->setToolTip(m_iwadsPath);
		QDirIterator it(path);
		while (it.hasNext()) {
			it.next();
			QString fileName = it.fileName();
			if (fileName.toLower().endsWith(".wad"))
				m_iwads->addItem(it.fileName());
		}
	}
}

void Launcher::populateMaps(const QString& iwad)
{
	m_mapName->clear();
	m_mapName->addItem(tr("Unspecified"));
	m_mapName->setCurrentIndex(0);

	if (!iwad.isEmpty()) {
		const QStringList &list = m_mapList.listForIwad(iwad);
		for (const QString &map : list)
			m_mapName->addItem(tr(qPrintable(map)));
	}
}

void Launcher::populateFileFolder(const QString &path)
{
	if (!path.isEmpty()) {
		FileTreeItem *root = new FileTreeItem;

		populateFileFolderRecursive(path, root);

		if (root->childCount() > 0) {
			m_filesModel->setText(root, 0, path);
			m_filesModel->addTopLevelItem(root);
		} else {
			delete root;
		}
	}
}

void Launcher::populateFileFolderRecursive(const QString &path, FileTreeItem *parent)
{
	QDir dir{path};
	dir.setFilter(QDir::NoDotAndDotDot | QDir::Files | QDir::Dirs);
	QFileInfoList fileList = dir.entryInfoList();

	for (QFileInfo fileInfo : fileList) {
		if (fileInfo.isFile()) {
			QString fileName = fileInfo.fileName();
			QString fileNameLowered = fileName.toLower();
			if (fileNameLowered.endsWith(".wad")
				|| fileNameLowered.endsWith(".pk3")
				|| fileNameLowered.endsWith(".ipk3")
				|| fileNameLowered.endsWith(".deh")
				|| fileNameLowered.endsWith(".bex"))
			{
				FileTreeItem *item = new FileTreeItem;
				m_filesModel->setText(item, 0, fileName);
				m_filesModel->setCheckState(item, 0, Qt::Unchecked);
				parent->addChild(item);
			}
		} else if (fileInfo.isDir()) {
			FileTreeItem *subDir = new FileTreeItem;

			populateFileFolderRecursive(fileInfo.filePath(), subDir);

			if (subDir->childCount() > 0) {
				m_filesModel->setText(subDir, 0, fileInfo.fileName());
				parent->addChild(subDir);
			} else {
				delete subDir;
			}
		}
	}
}

void Launcher::expandOrCollapseTree(FileTreeItem *tree, bool expand)
{
	m_filesModel->setExpanded(tree, expand);
	for (int i = 0; i < tree->childCount(); ++i) {
		FileTreeItem *child = tree->child(i);
		expandOrCollapseTree(child, expand);
	}
}

void Launcher::uncheckFileTree(FileTreeItem *tree)
{
	if (tree->checkState(0) == Qt::Checked)
		m_filesModel->setCheckState(tree, 0, Qt::Unchecked);
	for (int i = 0; i < tree->childCount(); ++i)
		uncheckFileTree(tree->child(i));
}

void Launcher::migrateStrictCheckStateToTriState(QByteArray& data)
{
	if (data.isEmpty())
		return;

	QString enginePath;
	QString iwad;
	qint8 skill;
	QString mapName;
	qint8 strictCheckState;
	QString commandLine;
	QList<QPair<QString, int>> files;

	{
		QDataStream stream{data};

		stream >> enginePath;
		stream >> iwad;
		stream >> skill;
		stream >> mapName;
		stream >> strictCheckState;
		stream >> commandLine;
		stream >> files;
	}

	strictCheckState = static_cast<qint8>(strictCheckState ? Qt::Checked : Qt::PartiallyChecked);

	{
		QDataStream stream{&data, QIODevice::WriteOnly};

		stream << enginePath;
		stream << iwad;
		stream << skill;
		stream << mapName;
		stream << strictCheckState;
		stream << commandLine;
		stream << files;
	}
}

#include "moc_Launcher.cpp"
