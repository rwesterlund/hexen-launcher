#pragma once

#ifndef MAPLIST_HPP
#define MAPLIST_HPP

#include <QString>
#include <QStringList>

class MapList {
public:
		MapList();

		const QStringList& listForIwad(const QString& iwad);
		QString warpForIwadAndListIndex(const QString& iwad, int listIndex);

		QStringList doomLevels;
		QStringList doom2Levels;
		QStringList tntLevels;
		QStringList plutoniaLevels;
		QStringList hereticLevels;
		QStringList hexenLevels;
		QStringList hexddLevels;
		QStringList strifeLevels;
		QStringList chexLevels;

		QStringList emptyList;
};

#endif // MAPLIST_HPP
