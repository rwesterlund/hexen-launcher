#pragma once

#ifndef FILETREEITEM_HPP
#define FILETREEITEM_HPP

#include <QVariant>
#include <QList>
#include <QStringList>

class QTreeView;
class FileTreeModel;

class FileTreeItem {
	friend class FileTreeModel;

public:
	FileTreeItem();
	~FileTreeItem();

	FileTreeItem *parent() const
	{
		return m_parent;
	}

	bool isTopLevel() const
	{
		return m_parent == nullptr || m_parent->parent() == nullptr;
	}

	int childCount() const
	{
		return m_children.count();
	}

	FileTreeItem *child(int index) const
	{
		return m_children[index];
	}

	void addChild(FileTreeItem *item);
	void removeChild(FileTreeItem *item);

	QVariant data(int column, int role) const;

	QString text(int column) const
	{
		return data(column, Qt::DisplayRole).toString();
	}

	Qt::CheckState checkState(int column) const
	{
		return static_cast<Qt::CheckState>(data(column, Qt::CheckStateRole).toInt());
	}

protected:
	// Called by a model, these methods are protected since they do not emit signals and finalize the operations by themselves.
	bool setData(int column, const QVariant &value, int role = Qt::EditRole);

	void setText(int column, const QString &atext)
	{
		setData(column, atext, Qt::DisplayRole);
	}

	void setCheckState(int column, Qt::CheckState state)
	{
		setData(column, state, Qt::CheckStateRole);
	}
	
	void setExpanded(bool expand);

	void sort(int column, Qt::SortOrder order = Qt::AscendingOrder);

private:
	void setViewRecursive(FileTreeItem *item, QTreeView *view);

	Qt::ItemFlags m_itemFlags;
	FileTreeItem *m_parent;
	QList<FileTreeItem *> m_children;
	QTreeView *m_view;
	QStringList m_texts;
	Qt::CheckState m_checkState;
};

#endif // FILETREEITEM_HPP
