#pragma once

#ifndef LAUNCHER_HPP
#define LAUNCHER_HPP

#include <QMainWindow>
#include <QString>
#include <QStringRef>
#include <QStringList>
#include <memory>
#include <random>
#include "MapList.hpp"

class QSplitter;
class QBoxLayout;
class QLabel;
class QSlider;
class QLineEdit;
class QComboBox;
class QPushButton;
class QCheckBox;
class QListWidget;
class QListWidgetItem;
class QStandardItemModel;
class QTreeView;
class QTreeViewItem;
class QSettings;
class QMenu;
class FileTreeModel;
class FileTreeItem;

class Launcher : public QMainWindow
{
    Q_OBJECT

public:
    explicit Launcher(QWidget *parent = 0);
    ~Launcher();

public slots:
    void runPressed(bool checked);
	void showCmdLinePressed(bool checked);
	void browseEnginePressed(bool checked);
	void iwadChanged(QListWidgetItem *current, QListWidgetItem *previous);
	void iwadsContextMenu(const QPoint &pos);
	void filesContextMenu(const QPoint &pos);
	void savedConfContextMenu(const QPoint &pos);
	void setIwadFolder();
	void setRelativeIwadFolder();

	void addFileFolder();
	void addRelativeFileFolder();
	void removeFileFolder();
	void expandFileTree();
	void collapseFileTree();
	void uncheckAllFiles();

	void saveNewConfiguration();
	void updateConfiguration();
	void removeConfiguration();
	void moveConfigurationUp();
	void moveConfigurationDown();

	void configurationDoubleClicked(QListWidgetItem *item);

	void showSettingsMenu();
	void showAboutWindow();

private:
	enum class EngineFamily {
		Unknown,
		ZDoom,
		Chocolate
	};

	void loadSettings();
	void loadSettingsUsingFile(QSettings &settings);
	void saveSettings();
	void saveSettingsUsingFile(QSettings &settings);

	// Returns true if a valid launch configuration could be generated, otherwise false.
	bool assembleCommandLine(QString &path, QStringList &arguments, QString &workingDirectory);

	EngineFamily engineFamilyForBinary(const QString &path);

	void setEnginePath(const QString &path);
	QString selectedIwad();
	QList<QPair<QString, int>> checkedFiles();
	void checkedFilesRecursive(FileTreeItem *item, QStringList &prefix, QList<QPair<QString, int>> &results);
	void checkFiles(QList<QPair<QString, int>> files, QStringList &rejected);
	bool checkFilesRecursive(FileTreeItem *item, QStringRef file, int priority);

	void applyConfiguration(const QByteArray& configuration, bool reportErrors);
	QByteArray calculateConfiguration();

	void populateIwads(const QString &path);
	void populateMaps(const QString& iwad);
	void populateFileFolder(const QString &path);
	void populateFileFolderRecursive(const QString &path, FileTreeItem *parent);
	void expandOrCollapseTree(FileTreeItem *tree, bool expand);
	void uncheckFileTree(FileTreeItem *tree);

	// Setting revision migration functions.
	void migrateStrictCheckStateToTriState(QByteArray& data);

	const char* organizationName = "HeXen Launcher";
	const char* applicationName = "HeXen Launcher";
	const unsigned int settingsRevision = 4;
	const char* localSettingsFileName = "settings.ini";

	std::default_random_engine m_randomGenerator;
	std::uniform_int_distribution<int> m_classRandomizer;

	std::unique_ptr<QWidget> m_centralWidget;
	std::unique_ptr<QBoxLayout> m_centralWidgetLayout;
	std::unique_ptr<QSplitter> m_columnSplitter;

	std::unique_ptr<QWidget> m_launchRow;
	std::unique_ptr<QBoxLayout> m_launchRowLayout;
	std::unique_ptr<QPushButton> m_launch;
	std::unique_ptr<QPushButton> m_showCmdLine;
    
    std::unique_ptr<QWidget> m_leftColumn;
    std::unique_ptr<QWidget> m_middleColumn;
    std::unique_ptr<QWidget> m_rightColumn;
    std::unique_ptr<QBoxLayout> m_leftColumnLayout;
    std::unique_ptr<QBoxLayout> m_middleColumnLayout;
    std::unique_ptr<QBoxLayout> m_rightColumnLayout;

	// Left column
	std::unique_ptr<QLabel> m_iwadsLabel;
	std::unique_ptr<QListWidget> m_iwads;

	std::unique_ptr<QBoxLayout> m_engineLayout;
	std::unique_ptr<QLabel> m_engineLabel;
	std::unique_ptr<QComboBox> m_enginePath;
	std::unique_ptr<QPushButton> m_browseEngine;

	std::unique_ptr<QBoxLayout> m_skillLayout;
	std::unique_ptr<QLabel> m_skillLabel;
	std::unique_ptr<QSlider> m_skillSlider;

	std::unique_ptr<QBoxLayout> m_mapLayout;
	std::unique_ptr<QLabel> m_mapLabel;
	std::unique_ptr<QComboBox> m_mapName;

	std::unique_ptr<QBoxLayout> m_classLayout;
	std::unique_ptr<QLabel> m_classLabel;
	std::unique_ptr<QComboBox> m_className;

	std::unique_ptr<QBoxLayout> m_parametersLayout;
	std::unique_ptr<QLabel> m_zdoomStrictLabel;
	std::unique_ptr<QCheckBox> m_zdoomStrictCheckBox;

	std::unique_ptr<QLabel> m_commandLineLabel;
	std::unique_ptr<QLineEdit> m_commandLine;

	// Middle column
	std::unique_ptr<QLabel> m_filesLabel;
	std::unique_ptr<QTreeView> m_files;
	FileTreeModel *m_filesModel;

	// Right column
	std::unique_ptr<QLabel> m_savedConfLabel;
	std::unique_ptr<QListWidget> m_savedConf;
	std::unique_ptr<QAction> m_savedConfMoveUpAction;
	std::unique_ptr<QAction> m_savedConfMoveDownAction;

	// Menu bar
	QMenu *m_settingsMenu;
	std::unique_ptr<QAction> m_settingPortableMode;
	std::unique_ptr<QAction> m_aboutAction;

	QString m_iwadsPath;
	MapList m_mapList;

	bool m_writeSettings;
};

#endif // #ifndef LAUNCHER_HPP
