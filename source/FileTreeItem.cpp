#include "FileTreeItem.hpp"
#include "FileTreeModel.hpp"
#include <QTreeView>
#include <algorithm>

FileTreeItem::FileTreeItem()
	: m_itemFlags(Qt::ItemIsSelectable | Qt::ItemIsEnabled),
	m_parent(nullptr),
	m_view(nullptr),
	m_checkState(Qt::Unchecked)
{
	for (int i = 0; i < 2; ++i)
		m_texts.append(QString());
}

FileTreeItem::~FileTreeItem()
{
	FileTreeModel *model = m_view ? static_cast<FileTreeModel *>(m_view->model()) : nullptr;
	if (model)
		m_parent->removeChild(this);

	while (!m_children.isEmpty())
		delete m_children.back();
	m_children.clear();

	if (m_parent) {
		m_parent->removeChild(this);
		m_parent = nullptr;
	}
}

void FileTreeItem::addChild(FileTreeItem *item)
{
	FileTreeModel *model = m_view ? static_cast<FileTreeModel *>(m_view->model()) : nullptr;
	if (model)
		model->beginInsertItems(item, m_children.count(), 1);
	m_children.append(item);
	setViewRecursive(item, m_view);
	item->m_parent = this;
	if (model)
		model->endInsertItems();
}

void FileTreeItem::removeChild(FileTreeItem *item)
{
	int childIndex = m_children.indexOf(item);
	if (childIndex != -1) {
		FileTreeModel *model = m_view ? static_cast<FileTreeModel *>(m_view->model()) : nullptr;
		if (model)
			model->beginRemoveItems(this, childIndex, 1);
		m_children.removeAt(childIndex);
		setViewRecursive(item, nullptr);
		item->m_parent = nullptr;
		if (model)
			model->endRemoveItems();
	}
}

QVariant FileTreeItem::data(int column, int role) const
{
	switch (role) {
	case Qt::DisplayRole:
		// Priority displays empty string for value 0.
		if (column == 1 && m_texts[column].isEmpty())
			return m_texts[column];
		// fallthrough
	case Qt::EditRole:
		if (column == 0)
			return m_texts[column];
		else if (column == 1)
			return m_texts[column].toInt();
		break;
	case Qt::CheckStateRole:
		if (column == 0)
			return m_checkState;
		break;
	}

	return QVariant();
}

bool FileTreeItem::setData(int column, const QVariant &value, int role)
{
	switch (role) {
	case Qt::DisplayRole:
	case Qt::EditRole:
		if (column == 0) {
			m_texts[column] = value.toString();
			return true;
		} else if (column == 1) {
			bool valid = false;
			int intValue = value.toInt(&valid);
			if (valid) {
				if (intValue == 0)
					m_texts[column] = QString();
				else
					m_texts[column] = QString::number(intValue);
				return true;
			} else if (value.toString().isEmpty()) {
				m_texts[column] = value.toString();
				return true;
			}
		}
		break;
	case Qt::CheckStateRole:
		if (column == 0) {
			m_checkState = static_cast<Qt::CheckState>(value.toInt());
			if (m_checkState == Qt::Unchecked) // Reset priority on uncheck.
				m_texts[1] = QString();
			return true;
		}
		break;
	}

	return false;
}

void FileTreeItem::setExpanded(bool expand)
{
	if (m_view) {
		FileTreeModel *model = static_cast<FileTreeModel *>(m_view->model());
		QModelIndex index = model->index(this, 0);
		m_view->setExpanded(index, expand);
	}
}

void FileTreeItem::sort(int column, Qt::SortOrder order)
{
	for (FileTreeItem *treeItem : m_children)
		treeItem->sort(column, order);

	std::sort(m_children.begin(), m_children.end(),
		[column, order] (FileTreeItem *left, const FileTreeItem *right) -> bool {
			// Directories are always sorted before files.
			if ((left->childCount() > 0) && (right->childCount() == 0))
				return true;
			else if (right->childCount() > 0)
				return false;

			if (order == Qt::AscendingOrder)
				return left->text(column) < right->text(column);
			return left->text(column) > right->text(column);
		});
}

void FileTreeItem::setViewRecursive(FileTreeItem *item, QTreeView *view)
{
	if (item->m_view != view) {
		item->m_view = view;
		for (int i = 0; i < item->childCount(); ++i)
			setViewRecursive(item->child(i), view);
	}
}
