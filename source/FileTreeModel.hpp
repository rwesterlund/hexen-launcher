#pragma once

#ifndef FILETREEMODEL_HPP
#define FILETREEMODEL_HPP

#include "FileTreeItem.hpp"
#include <QAbstractItemModel>
#include <QModelIndex>

class FileTreeModel : public QAbstractItemModel {
	Q_OBJECT
	friend class FileTreeItem;

public:
	explicit FileTreeModel(QTreeView *view);
	~FileTreeModel();

	FileTreeItem *item(const QModelIndex &index) const;
	QModelIndex index(int row, int column, const QModelIndex &parent = QModelIndex()) const override;
	QModelIndex index(const FileTreeItem *item, int column) const;
	QModelIndex parent(const QModelIndex &index) const override;
	int rowCount(const QModelIndex &parent = QModelIndex()) const override;
	int columnCount(const QModelIndex &parent = QModelIndex()) const override;
	QVariant data(const QModelIndex &index, int role) const override;
	bool setData(const QModelIndex &index, const QVariant &value, int role = Qt::EditRole) override;
	QVariant headerData(int section, Qt::Orientation orientation, int role = Qt::DisplayRole) const override;
	bool setHeaderData(int section, Qt::Orientation orientation, const QVariant &value, int role = Qt::EditRole) override;
	Qt::ItemFlags flags(const QModelIndex &index) const override;

	int topLevelItemCount()
	{
		return m_topLevelItem.childCount();
	}

	FileTreeItem *topLevelItem(int index)
	{
		return m_topLevelItem.child(index);
	}

	void addTopLevelItem(FileTreeItem *item)
	{
		m_topLevelItem.addChild(item);
	}

	QString text(FileTreeItem *treeItem, int column) const
	{
		return data(index(treeItem, column), Qt::DisplayRole).toString();
	}

	void setText(FileTreeItem *treeItem, int column, const QString &atext)
	{
		treeItem->setData(column, atext, Qt::DisplayRole);
		emitDataChange(treeItem, column, atext, Qt::DisplayRole);
	}

	Qt::CheckState checkState(FileTreeItem *treeItem, int column) const
	{
		return static_cast<Qt::CheckState>(data(index(treeItem, column), Qt::CheckStateRole).toInt());
	}

	void setCheckState(FileTreeItem *treeItem, int column, Qt::CheckState state)
	{
		treeItem->setData(column, state, Qt::CheckStateRole);
		emitDataChange(treeItem, column, state, Qt::CheckStateRole);
	}

	void setExpanded(FileTreeItem *treeItem, bool expand)
	{
		treeItem->setExpanded(expand);
	}

	void sort() { sort(m_sortColumn, m_sortOrder); }
	void sort(int column, Qt::SortOrder order = Qt::AscendingOrder) override;

private:
	void beginInsertItems(FileTreeItem *parent, int row, int count);
	void endInsertItems();
	void beginRemoveItems(FileTreeItem *parent, int row, int count);
	void endRemoveItems();

	void emitDataChange(const QModelIndex &index, const QVariant &value, int role);
	void emitDataChange(FileTreeItem *treeItem, int column, const QVariant &value, int role)
	{
		QModelIndex itemIndex = index(treeItem, column);
		if (itemIndex.isValid())
			emitDataChange(itemIndex, value, role);
	}

	int m_sortColumn = 0;
	Qt::SortOrder m_sortOrder = Qt::AscendingOrder;
	FileTreeItem m_topLevelItem;
	QVariant m_headerData[2];
};

#endif // FILETREEMODEL_HPP
