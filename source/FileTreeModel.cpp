#include "FileTreeModel.hpp"

FileTreeModel::FileTreeModel(QTreeView *view)
	: QAbstractItemModel(nullptr)
{
	m_topLevelItem.m_view = view;
}

FileTreeModel::~FileTreeModel()
{
}

FileTreeItem *FileTreeModel::item(const QModelIndex &index) const
{
	if (!index.isValid())
		return nullptr;
	return static_cast<FileTreeItem *>(index.internalPointer());
}

QModelIndex FileTreeModel::index(int row, int column, const QModelIndex &parent) const
{
	if (row < 0 || column < 0 || columnCount(parent) <= column)
		return QModelIndex();

	const FileTreeItem *parentItem = parent.isValid() ? item(parent) : &m_topLevelItem;
	if (!parentItem)
		parentItem = &m_topLevelItem;
	if (row < parentItem->childCount()) {
		FileTreeItem *childItem = parentItem->child(row);
		if (childItem)
			return createIndex(row, column, childItem);
	}

 	return QModelIndex();
}

QModelIndex FileTreeModel::index(const FileTreeItem *item, int column) const
{
	if (!item || item == &m_topLevelItem)
		return QModelIndex();

	const FileTreeItem *parentItem = item->parent();
	if (!parentItem)
		parentItem = &m_topLevelItem;

	int childIndex = parentItem->m_children.indexOf(const_cast<FileTreeItem *>(item));
	if (childIndex != -1)
		return createIndex(childIndex, column, const_cast<FileTreeItem *>(item));

	return QModelIndex();
}

QModelIndex FileTreeModel::parent(const QModelIndex &index) const
{
	if (!index.isValid())
		return QModelIndex();

	FileTreeItem *itm = static_cast<FileTreeItem *>(index.internalPointer());
	if (!itm || itm == &m_topLevelItem)
		return QModelIndex();
	FileTreeItem *parent = itm->parent();
	return this->index(parent, 0);
}

int FileTreeModel::rowCount(const QModelIndex &parent) const
{
	if (!parent.isValid())
		return m_topLevelItem.childCount();

	FileTreeItem *parentItem = item(parent);
	if (parentItem)
		return parentItem->childCount();
	return 0;
}

int FileTreeModel::columnCount(const QModelIndex &parent) const
{
	return 2;
}

QVariant FileTreeModel::data(const QModelIndex &index, int role) const
{
	FileTreeItem *childItem = item(index);
	if (childItem)
		return childItem->data(index.column(), role);

	return QVariant();
}

bool FileTreeModel::setData(const QModelIndex &index, const QVariant &value, int role)
{
	FileTreeItem *treeItem = item(index);

	if (treeItem) {
		if (treeItem->setData(index.column(), value, role)) {
			emitDataChange(index, value, role);
			return true;
		}
	}

	return false;
}

QVariant FileTreeModel::headerData(int section, Qt::Orientation orientation, int role) const
{
	if (section >= 0 && section < 2) {
		switch (role) {
		case Qt::DisplayRole:
		case Qt::EditRole:
			return m_headerData[section];
			break;
		}
	}

	return QVariant();
}

bool FileTreeModel::setHeaderData(int section, Qt::Orientation orientation, const QVariant &value, int role)
{
	if (section >= 0 && section < 2) {
		switch (role) {
		case Qt::DisplayRole:
		case Qt::EditRole:
			m_headerData[section] = value;
			emit headerDataChanged(orientation, section, section);
			return true;
			break;
		}
	}

	return false;
}

Qt::ItemFlags FileTreeModel::flags(const QModelIndex &index) const
{
	FileTreeItem *treeItem = item(index);

	if (treeItem) {
		Qt::ItemFlags itemFlags = treeItem->m_itemFlags;
		if (index.column() == 0)
			itemFlags |= Qt::ItemIsUserCheckable;
		if (index.column() == 1 && treeItem->m_checkState != Qt::Unchecked)
			itemFlags |= Qt::ItemIsEditable;
		return itemFlags;
	}

	return 0;
}

void FileTreeModel::sort(int column, Qt::SortOrder order)
{
	m_sortColumn = column;
	m_sortOrder = order;
	m_topLevelItem.sort(column, order);
	emit layoutChanged();
}

void FileTreeModel::beginInsertItems(FileTreeItem *parent, int row, int count)
{
	beginInsertRows(index(parent, 0), row, row + count - 1);
}

void FileTreeModel::endInsertItems()
{
	endInsertRows();
	sort();
}

void FileTreeModel::beginRemoveItems(FileTreeItem *parent, int row, int count)
{
	beginRemoveRows(index(parent, 0), row, row + count - 1);
}

void FileTreeModel::endRemoveItems()
{
	endRemoveRows();
	sort();
}

void FileTreeModel::emitDataChange(const QModelIndex &index, const QVariant &value, int role)
{
	QVector<int> roles;
	roles.append(role);
	if (role == Qt::CheckStateRole) {
		// If unchecked, priority field has also been altered.
		roles.append(Qt::DisplayRole);
		QModelIndex endIndex = createIndex(index.row(), 1, index.internalPointer());
		emit dataChanged(index, endIndex, roles);
	} else {
		emit dataChanged(index, index, roles);
	}
}
